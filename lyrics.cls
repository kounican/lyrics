\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{lyrics}

\LoadClass[onecolumn]{article}

\usepackage[margin=1in]{geometry}
\usepackage{multicol}
\usepackage{ifthen}
\setlength{\columnsep}{0.5in}



\newcommand{\rep}[2][]{
  $\mid$: #2 :$\mid$
  \ifthenelse{\equal{#1}{}}{}{(#1)}\\
}

\newcommand{\strophe}[1]{#1 \newline}

\newcommand{\song}[3][]{
  \begin{samepage}
    \subsection{#2}
    \ifthenelse{\equal{#1}{}}{#3}{\href{#1}{(video)}\\{#3}}
  \end{samepage}
}

\newcommand{\group}[2]{
  \newpage
  \begin{multicols*}{2}
    [\begin{center}
      \section{#1}
      \vspace{0.5cm}
    \end{center}]
    {#2}
  \end{multicols*}
}
